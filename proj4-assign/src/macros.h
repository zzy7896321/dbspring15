#ifndef __MY_MACROS_H
#define __MY_MACROS_H

#define CONCATHELPER(s1,s2) s1##s2
#define CONCAT(s1, s2) CONCATHELPER(s1,s2)

#define STRINGIFYHELPER(m) #m
#define STRINGIFY(num) STRINGIFYHELPER(num)

#ifdef __GNUC__
#	define likely(expr) (__builtin_expect(!!(expr), 1))
#	define unlikely(expr) (__builtin_expect(!!(expr), 0))
#else
#	define likely(expr) (expr)
#	define unlikely(expr) (expr)
#endif

#endif
