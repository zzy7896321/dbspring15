// file sort.C  --  implementation of Sort class.

#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <algorithm>
#include <utility>
using std::swap;

#include "db.h"
#include "sort.h"
#include "heapfile.h"
#include "system_defs.h"
#include "macros.h"

#ifndef ZZY_DEBUG
#	define debug(...) 
#else
#	define debug(fmt, ...)	\
	printf("[DEBUG] " fmt "\n", ##__VA_ARGS__)
#endif

#define MAX_TMP_SUFFIX_LEN 10
#define NAME_BUF_SIZE (MAX_NAME + 1)
#define MINIMUM_SORT_BUF 3

#define SAFE_CALL_CLEAN(expr, clean_block)	\
	do {	\
		Status __status = (expr);	\
		if (__status != OK) {	\
			clean_block	\
			return MINIBASE_CHAIN_ERROR(SORT, __status);	\
		}	\
	} while(0)
	
#define SAFE_CALL(expr)	\
	SAFE_CALL_CLEAN(expr, /* nothing to clean */)

// Add you error message here
static const char *sortErrMsgs[SORT_NERRS] = {
	"file name is too long",
	"too many tmp files",
	"invalid argument",
	"out of memory",
	"buffer overflow",

	"fatal error",
};
static error_string_table sortTable(SORT, sortErrMsgs);

typedef int (*__qsort_r_cmp_fn)(const void*, const void*, void*);

typedef struct __sort_context_t {
	/* arguments passed to Sort::Sort */
	char *in_file;
	char *out_file;
	int len_in;
	AttrType *in;
	short *str_sizes;
	int fld_no;
	TupleOrder sort_order;
	int amt_of_buf;
	
	/* states filled in by sort_file */
	int in_file_len;	/* length of the input file*/
	int tmp_file_no;	/* next no. of tmp file */
	ptrdiff_t sf_off;	/* offset of the key field */
	size_t sf_size;		/* sizeof the key field */
	__qsort_r_cmp_fn s_cmp;	/* cmp function of qsort */
	size_t s_entry;		/* size of each entry */

	/* states across passes */
	int run_id_len;
	int n_runs;
	int *run_id;
	
	/* states in merge phases */
	int old_scan_head;	/* 1 past the last old runs scanned */
	int new_scan_tail;	/* 1 past the last new runs created */
} __sctx_t;

#ifdef ZZY_DEBUG
static char dump_buffer[PAGESIZE + 10];
static char* dump_key(__sctx_t *sctx, char *entry) {
	switch (sctx->in[sctx->fld_no]) {
	case attrInteger:
		snprintf(dump_buffer, PAGESIZE + 10, "%d", *(int*) (entry + sctx->sf_off));
		break;
	case attrString:
		snprintf(dump_buffer, PAGESIZE + 10, "%s", (entry + sctx->sf_off));
		break;
	default:
		snprintf(dump_buffer, PAGESIZE + 10, "<unsupported key type>");
	}
	return dump_buffer;
}
#endif

static void init_sort_context(__sctx_t* sctx, 
	char* inFile, char* outFile, int len_in, AttrType in[],
	     short str_sizes[], int fld_no, TupleOrder sort_order,
	     int amt_of_buf) {
	memset(sctx, 0, sizeof(*sctx));
	sctx->in_file = inFile;
	sctx->out_file = outFile;
	sctx->len_in = len_in;
	sctx->in = in;
	sctx->str_sizes = str_sizes;
	sctx->fld_no = fld_no;
	sctx->sort_order = sort_order;
	sctx->amt_of_buf = amt_of_buf;
}

static inline Status sort_get_tmp_file_name(__sctx_t* sctx, int id, char* tfname) {
	int n = snprintf(tfname, NAME_BUF_SIZE, 
			"%s%" STRINGIFY(MAX_TMP_SUFFIX_LEN) "d", sctx->in_file, id);
	if (n >= (int) NAME_BUF_SIZE) {
		return MINIBASE_FIRST_ERROR(SORT, SORT_TOOLONG_FNAME);
	}
	return OK;
}

static Status sort_get_next_tmp_file_name(__sctx_t* sctx, char *tfname) {
	PageId unused;
	Status status;
	
	for (; ++sctx->tmp_file_no >= 0; ) {
		SAFE_CALL(sort_get_tmp_file_name(sctx, sctx->tmp_file_no, tfname));
			
		status = MINIBASE_DB->get_file_entry(tfname, unused);
		if (status != OK) {
			/* DBMGR does not really follow the error protocol!!
			 * This is actually file not found. */
			return OK;
		}
	}

	return MINIBASE_FIRST_ERROR(SORT, SORT_TMP_FOFLOW);
}

/* true if there's fatal error */
static bool inline free_tmp_file(__sctx_t *sctx, int id) {
	Status status;
	char fname[NAME_BUF_SIZE];
	if (sort_get_tmp_file_name(sctx, sctx->run_id[--sctx->n_runs], fname) != OK) {
		return true;
	}
	HeapFile hf(fname, status);
	if (status != OK) {
		return true;
	}
	status = hf.deleteFile();
	if (status != OK) {
		return true;
	}
	return false;
}

static void free_sort_context(__sctx_t* sctx) {
	/* ignore the errors, but report a fatal error resulted from them */
	bool fatal = false;
	if (sctx->run_id) {
		for (; sctx->n_runs > sctx->old_scan_head; ) {
			fatal |= free_tmp_file(sctx, --sctx->n_runs);
		}
		for (; sctx->new_scan_tail > 0; ) {
			fatal |= free_tmp_file(sctx, --sctx->new_scan_tail);
		}
		free(sctx->run_id);
	}

	if (fatal)
		MINIBASE_RESULTING_ERROR(SORT, minibase_errors.status(), SORT_FATAL);
}

#define DEFINE_CMP_FUNC(name)	\
static int __sort_cmp_##name##_asc(	\
		const void* key1, const void* key2,	\
		void* ctx)	{	\
	__sctx_t* sctx = (__sctx_t*) ctx;	\
	return key_cmp_func(	\
			(((char*) key1) + sctx->sf_off),	\
			(((char*) key2) + sctx->sf_off));	\
}	\
	\
static int __sort_cmp_##name##_desc(	\
		const void* key1, const void* key2,	\
		void* ctx)	{	\
	__sctx_t* sctx = (__sctx_t*) ctx;	\
	return -key_cmp_func(	\
			(((char*) key1) + sctx->sf_off),	\
			(((char*) key2) + sctx->sf_off));	\
}	\

static inline int __sort_int_cmp(void* key1, void* key2) {
	int* ik1 = (int*) key1;
	int* ik2 = (int*) key2;
	if (*ik1 < *ik2) return -1;
	return (*ik1 > *ik2) ? 1 : 0;
}

#define key_cmp_func(k1, k2) __sort_int_cmp(k1, k2)
DEFINE_CMP_FUNC(int)
#undef key_cmp_func

#define key_cmp_func(k1, k2) strncmp(k1, k2, sctx->sf_size)
DEFINE_CMP_FUNC(str)
#undef key_cmp_func

#undef DEFINE_CMP_FUNC

#define CMP_FUNC(name, order) (	\
	((order) == Ascending) ? __sort_cmp_##name##_asc :	\
	((order) == Descending) ? __sort_cmp_##name##_desc :	\
	0)


static Status sort_file_check_args(__sctx_t* sctx) {
	sctx->tmp_file_no = -1;

	sctx->in_file_len = strnlen(sctx->in_file, NAME_BUF_SIZE);
	if (sctx->in_file_len == 0 || 
		sctx->in_file_len + MAX_TMP_SUFFIX_LEN >= (int) NAME_BUF_SIZE) {
		return MINIBASE_FIRST_ERROR(SORT, SORT_TOOLONG_FNAME);
	}
	
	int out_file_len = strnlen(sctx->out_file, NAME_BUF_SIZE);
	if (out_file_len == 0 || out_file_len >= (int) NAME_BUF_SIZE) {
		return MINIBASE_FIRST_ERROR(SORT, SORT_TOOLONG_FNAME);
	}

	if (sctx->len_in <= 0 || !sctx->in || !sctx->str_sizes || 
		sctx->fld_no < 0 || sctx->fld_no >= sctx->len_in ||
		sctx->amt_of_buf < MINIMUM_SORT_BUF) {
		
		return MINIBASE_FIRST_ERROR(SORT, SORT_INVAL);
	}

	sctx->sf_off = 0;
	sctx->s_entry = 0;

	for (int i = 0; i < sctx->len_in; ++i) {
		if (sctx->str_sizes[i] < 0) 
			return MINIBASE_FIRST_ERROR(SORT, SORT_INVAL);
		sctx->s_entry += sctx->str_sizes[i];
		if (i < sctx->fld_no) {
			sctx->sf_off += sctx->str_sizes[i];	
		}
	}
	sctx->sf_size = (size_t) sctx->str_sizes[sctx->fld_no];
	
	/* entry size == 0 is an error because we actually
	 * have nothing to sort */
	if (sctx->s_entry == 0)
		return MINIBASE_FIRST_ERROR(SORT, SORT_INVAL);

	if ((int) sctx->s_entry > PAGESIZE * sctx->amt_of_buf) {
		return MINIBASE_FIRST_ERROR(SORT, SORT_INVAL);
	}

	switch (sctx->in[sctx->fld_no]) {
	case attrInteger:
		sctx->s_cmp = CMP_FUNC(int, sctx->sort_order);
		break;
	case attrString:
		sctx->s_cmp = CMP_FUNC(str, sctx->sort_order);
		break;
	default:
		return MINIBASE_FIRST_ERROR(SORT, SORT_INVAL);
	}

	if (!sctx->s_cmp)
		return MINIBASE_FIRST_ERROR(SORT, SORT_INVAL);
	
	return OK;
}

static Status sort_dump_buffer_to_file(
		__sctx_t *sctx,
		char *buffer,
		ptrdiff_t buf_off,
		HeapFile *hf) {

	RID unused;	
	for (ptrdiff_t i = 0; i < buf_off; i += sctx->s_entry) {
		debug("\t%s", dump_key(sctx, &buffer[i]));
		SAFE_CALL(hf->insertRecord(&buffer[i], sctx->s_entry, unused));
	}
	return OK;
}


static Status sort_first_pass(__sctx_t *sctx) {
	Status status;

	HeapFile hpfile(sctx->in_file, status);
	SAFE_CALL(status);
	
	const unsigned max_size = sctx->amt_of_buf * PAGESIZE;
	const int n_ent_per_run = max_size / sctx->s_entry;
	const int n_recs = hpfile.getRecCnt();
	const int n_runs = (n_recs + n_ent_per_run - 1) / n_ent_per_run;
	const bool last_pass = n_runs <= 1;
	
	debug("max_size = %d", max_size);
	debug("n_ent_per_run = %d", n_ent_per_run);
	debug("n_recs = %d", n_recs);
	debug("n_runs = %d", n_runs);
	debug("last_pass = %d", last_pass);
	
	if (n_runs == 0) {
		/* just create a new file */
		delete new HeapFile(sctx->out_file, status);
		SAFE_CALL(status);
		return OK;
	}
	
	if (!last_pass) {
		sctx->run_id = (int*) malloc(sizeof(int) * n_runs);
		sctx->run_id_len = n_runs;
	}

	char *buffer = (char*) malloc(max_size);
	if (!buffer) {
		return MINIBASE_FIRST_ERROR(SORT, SORT_NO_MEM);
	}

	Scan *scan = hpfile.openScan(status);
	SAFE_CALL_CLEAN(status, free(buffer); );

	int rec_len;
	ptrdiff_t buf_off = 0;
	int now_rec = 0;
	RID unused_rid;
	char tmp_fname[NAME_BUF_SIZE];
	sctx->n_runs = 0;
	while (now_rec < n_recs || buf_off) {
		if (now_rec == n_recs || 
			buf_off + sctx->s_entry > max_size) {
			/* sort the buffer and write to disk */
			qsort_r(buffer, 
				buf_off / sctx->s_entry, 
				sctx->s_entry,
				sctx->s_cmp,
				sctx);

			if (last_pass) {
				/* last pass, write to output directly */
				if (now_rec != n_recs) {
					free(buffer);
					delete scan;
					return MINIBASE_FIRST_ERROR(SORT, SORT_BUFOFLOW);
				}

				HeapFile out_file(sctx->out_file, status);
				SAFE_CALL_CLEAN(status, { free(buffer); delete scan; });

				debug("last pass");
				SAFE_CALL_CLEAN(
						sort_dump_buffer_to_file(sctx, buffer, buf_off, &out_file), 
						{ free(buffer); delete scan; });	
			}
			else {
				/* write to temporary files */
				SAFE_CALL_CLEAN(
						sort_get_next_tmp_file_name(sctx, tmp_fname),
						{ free(buffer); delete scan; });

				if (sctx->n_runs >= sctx->run_id_len) {
					return MINIBASE_FIRST_ERROR(SORT, SORT_BUFOFLOW);
				}

				HeapFile tmp_file(tmp_fname, status);
				SAFE_CALL_CLEAN(status, { free(buffer); delete scan; });

				sctx->run_id[sctx->n_runs++] = sctx->tmp_file_no;

				debug("run %d", sctx->n_runs);
				SAFE_CALL_CLEAN(
						sort_dump_buffer_to_file(sctx, buffer, buf_off, &tmp_file), 
						{ free(buffer); delete scan; });	
			}

			buf_off = 0;
		}
		else {
			/* get next tuple */
			SAFE_CALL_CLEAN(
				scan->getNext(unused_rid, &buffer[buf_off], rec_len),
				{ free(buffer); delete scan; }
			);
			if (rec_len > (int) sctx->s_entry) {
				free(buffer);
				delete scan;
				return MINIBASE_FIRST_ERROR(SORT, SORT_BUFOFLOW);
			}
			buf_off += sctx->s_entry;
			++now_rec;
		}
	}

	free(buffer);
	delete scan;
	
	sctx->old_scan_head = sctx->new_scan_tail = 0;
	
	return OK;
}

static inline void sort_subsequent_pass_clear_resource(
		__sctx_t *sctx,
		HeapFile **hf,
		Scan **scan,
		char *entry_buf,
		int n) {
	Status status;
	bool fatal = false;
	for (int i = 0; i < n; ++i) {
		delete scan[i];

		status = hf[i]->deleteFile(); /* ignored */
		if (status != OK) {
			fatal = true;
		}
		delete hf[i];
	}

	if (fatal)
		MINIBASE_RESULTING_ERROR(SORT, minibase_errors.status(), SORT_FATAL);
}

static inline void sort_subsequent_pass_abort(
		__sctx_t *sctx,
		HeapFile **hf,
		Scan **scan,
		char *entry_buf,
		int n) {
	sort_subsequent_pass_clear_resource(sctx, hf, scan, entry_buf, n);
	delete [] scan;
	delete [] hf;
	delete [] entry_buf;
}

static inline Status sort_subsequent_pass_open_run(
		__sctx_t *sctx,
		HeapFile **hf,
		Scan **scan) {
	
	Status status;
	char tmp_name[NAME_BUF_SIZE];

	SAFE_CALL(sort_get_tmp_file_name(sctx, 
				sctx->run_id[sctx->old_scan_head], tmp_name));

	*hf = new HeapFile(tmp_name, status);
	SAFE_CALL(status);

	*scan = (*hf)->openScan(status);
	SAFE_CALL_CLEAN(status, { delete *hf; });

	++sctx->old_scan_head;

	return OK;
}

static Status sort_subsequent_pass_do_merge(
		__sctx_t *sctx,
		HeapFile **hf,
		Scan **scan,
		char *entry_buf,
		int n,
		HeapFile *out_file) {
	
	Status status;
	RID unused_rid;
	int unused_len;

	/* Scan::position is not properly implemented. Once we call
	 * getNext and the returned record is on the boundary of the
	 * page, we are not able to put back the record reliably.
	 * As a consequence, we have to cache
	 * the retrieved entries from the heads of all runs, which
	 * requires additional memory propotional to the amt_of_buf.
	 * The overhead is amt_of_buf - 1 pages in worst case. 
	 *
	 * entry_buf[i * (sctx->s_entry + 1) + sctx->s_entry] == 0, 
	 * if DONE is returned from scan[i]. It's nonzero, o.w.
	 * */
	const int skip_size = sctx->s_entry + 1;

	for (int i = 0; i < n; ++i) {
		/* not checking len == sctx->s_entry, since it's
		 * checked during pass 1 */
		status = scan[i]->getNext(unused_rid, 
				&entry_buf[i * skip_size], unused_len);
		if (status != OK) {
			if (status != DONE) {
				return MINIBASE_CHAIN_ERROR(SORT, status);
			}
			entry_buf[(i + 1) * skip_size - 1] = (char) 0;
		}
		else {
			entry_buf[(i + 1) * skip_size - 1] = (char) 1;
		}
	}

	for (;;) {
		int min_i = -1;
		for (int i = 0; i < n; ++i) {
			if (entry_buf[(i + 1) * (sctx->s_entry + 1) - 1]) {
				if (min_i == -1 || sctx->s_cmp(
						&entry_buf[i * skip_size],
						&entry_buf[min_i * skip_size],
						sctx) < 0) {
					min_i = i;
				}
			}
		}

		if (min_i == -1) break; /* we're done. */
		
		debug("\t%s", dump_key(sctx, &entry_buf[min_i * skip_size]));
		SAFE_CALL(out_file->insertRecord(
					&entry_buf[min_i * skip_size], sctx->s_entry, unused_rid));

		status = scan[min_i]->getNext(unused_rid,
				&entry_buf[min_i * skip_size], unused_len);
		if (status != OK) {
			if (status != DONE) {
				return MINIBASE_CHAIN_ERROR(SORT, status);
			}
			entry_buf[(min_i + 1) * skip_size - 1] = (char) 0;
		}
	}

	return OK;
}

static Status sort_subsequent_pass(__sctx_t *sctx) {
	Status status;
	const bool last_pass = sctx->n_runs + 1 <= sctx->amt_of_buf;

	debug("last_pass = %d\n", last_pass);

	HeapFile **hf = new HeapFile*[sctx->amt_of_buf - 1];
	if (!hf) {
		return MINIBASE_FIRST_ERROR(SORT, SORT_NO_MEM);
	}

	Scan **scan = new Scan*[sctx->amt_of_buf - 1];
	if (!scan) {
		delete [] scan;
		return MINIBASE_FIRST_ERROR(SORT, SORT_NO_MEM);
	}

	char *entry_buf = new char[(sctx->amt_of_buf - 1) * (sctx->s_entry + 1)];
	if (!entry_buf) {
		delete [] scan;
		delete [] hf;
		return MINIBASE_FIRST_ERROR(SORT, SORT_NO_MEM);
	}
	
	char fname[NAME_BUF_SIZE];
	int r_now = 0;
	while (sctx->old_scan_head < sctx->n_runs || r_now) {
		if (sctx->old_scan_head == sctx->n_runs /* no more runs*/ 
			|| r_now + 1 == sctx->amt_of_buf /* buf full */
			) {
			/* do merge and write to file */
			if (last_pass) {
				/* directly write to output file */
				if (sctx->old_scan_head != sctx->n_runs) {
					sort_subsequent_pass_abort(sctx, hf, scan, entry_buf, r_now);
					return MINIBASE_FIRST_ERROR(SORT, SORT_BUFOFLOW);
				}

				HeapFile out_file(sctx->out_file, status);
				SAFE_CALL_CLEAN(status,
					sort_subsequent_pass_abort(sctx, hf, scan, entry_buf, r_now););
				
				debug("last pass");
				SAFE_CALL_CLEAN(
					sort_subsequent_pass_do_merge(sctx, hf, scan, entry_buf, r_now, &out_file),
					sort_subsequent_pass_abort(sctx, hf, scan, entry_buf, r_now););
			}
			else {
				/* write to tmp file */
				SAFE_CALL_CLEAN(
					sort_get_next_tmp_file_name(sctx, fname),
					sort_subsequent_pass_abort(sctx, hf, scan, entry_buf, r_now););

				HeapFile tmp_file(fname, status);
				SAFE_CALL_CLEAN(status,
					sort_subsequent_pass_abort(sctx, hf, scan, entry_buf, r_now););
	
				/* save the id of the new tmp file, in case it's lost in the
				 * do_merge function */
				sctx->run_id[sctx->new_scan_tail++] = sctx->tmp_file_no;
				
				debug("run %d", sctx->new_scan_tail);
				SAFE_CALL_CLEAN(
					sort_subsequent_pass_do_merge(sctx, hf, scan, entry_buf, r_now, &tmp_file),
					sort_subsequent_pass_abort(sctx, hf, scan, entry_buf, r_now););
			}

			sort_subsequent_pass_clear_resource(sctx, hf, scan, entry_buf, r_now);
			r_now = 0;
		}

		else {
			/* open next run */
			SAFE_CALL_CLEAN(
				sort_subsequent_pass_open_run(sctx, hf+r_now, scan+r_now),
				sort_subsequent_pass_abort(sctx, hf, scan, entry_buf, r_now); );
			++r_now;
		}
	}

	delete [] scan;
	delete [] hf;
	delete [] entry_buf;

	if (last_pass) {
		/* clear up */
		sctx->n_runs = 0;
	}
	else {
		sctx->n_runs = sctx->new_scan_tail;
	}
	sctx->old_scan_head = sctx->new_scan_tail = 0;

	return OK;
}

static Status sort_file(__sctx_t* sctx) {
	SAFE_CALL_CLEAN(sort_file_check_args(sctx), free_sort_context(sctx););

	debug("pass 1");
	SAFE_CALL_CLEAN(sort_first_pass(sctx), free_sort_context(sctx););
	
#ifdef ZZY_DEBUG
	int pass = 1;
#endif
	while (sctx->n_runs > 0) {
		debug("");
		debug("pass %d", ++pass);
		SAFE_CALL_CLEAN(sort_subsequent_pass(sctx), free_sort_context(sctx););
	}

	free_sort_context(sctx);
	return OK;
}


// constructor.
Sort::Sort ( char* inFile, char* outFile, int len_in, AttrType in[],
	     short str_sizes[], int fld_no, TupleOrder sort_order,
	     int amt_of_buf, Status& status ) {
	__sctx_t sctx;
	
	init_sort_context(&sctx,
			inFile, outFile, len_in,
			in, str_sizes, fld_no, sort_order,
			amt_of_buf);
	status = sort_file(&sctx);

	return ;
}

#undef CMP_FUNC

#undef SAFE_CALL

