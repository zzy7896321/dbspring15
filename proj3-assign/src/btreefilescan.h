/*
 * btreefilescan.h
 *
 * sample header file
 *
 */
 
#ifndef _BTREEFILESCAN_H
#define _BTREEFILESCAN_H

#include "btfile.h"

// errors from this class should be defined in btfile.h

class BTreeFileScan : public IndexFileScan {
public:
    friend class BTreeFile;

    // get the next record
    Status get_next(RID & rid, void* keyptr);

    // delete the record currently scanned
    Status delete_current();

    // size of the key
	int keysize() { return key_size; }

    // destructor
    ~BTreeFileScan();
private:
	
	/**
	 * Scan in [from .. to).
	 */
	BTreeFileScan(int key_size, RID from, const void* hi_key, AttrType key_type);
	
	int key_size;
	AttrType key_type;
	RID cur;
	void* hi_key;
	BTLeafPage *leaf;
	bool current_deleted;
	bool dirty;
};

#endif
