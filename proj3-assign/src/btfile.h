/*
 * btfile.h
 *
 * sample header file
 *
 */
 
#ifndef _BTREE_H
#define _BTREE_H

#include "btindex_page.h"
#include "btleaf_page.h"
#include "index.h"
#include "btreefilescan.h"
#include "bt.h"

#include <string>

// Define your error code for B+ tree here
enum btErrCodes {
	BTREE_HEADER_KEYSIZE_MISMATCH = 0,		
	BTREE_HEADER_KEYTYPE_MISMATCH,
	BTREE_HEADER_INCORRECT_MAGIC,
	BTREE_UNINITIALISED,

	BTREE_FATAL_ERROR,

	BTREE_UNRECOGNIZED_PAGE,
	BTREE_FLUSH_HEADER_FAILED,
	BTREE_DELETION_FAILED, /* index deletion */
	BTREE_NO_RECORD_TO_DELETE,

	BTREE_NUM_ERRORS,
};

class BTreeFile: public IndexFile
{
  public:
    BTreeFile(Status& status, const char *filename);
    // an index with given filename should already exist,
    // this opens it.
    
    BTreeFile(Status& status, const char *filename, const AttrType keytype,  \
	      const int keysize);
    // if index exists, open it; else create it.
    
    ~BTreeFile();
    // closes index
    
    Status destroyFile();
    // destroy entire index file, including the header page and the file entry
    
    Status insert(const void *key, const RID rid);
    // insert <key,rid> into appropriate leaf page
    
    Status Delete(const void *key, const RID rid);
    // delete leaf entry <key,rid> from the appropriate leaf
    // you need not implement merging of pages when occupancy
    // falls below the minimum threshold (unless you want extra credit!)
    
    IndexFileScan *new_scan(const void *lo_key = NULL, const void *hi_key = NULL);
    // create a scan with given keys
    // Cases:
    //      (1) lo_key = NULL, hi_key = NULL
    //              scan the whole index
    //      (2) lo_key = NULL, hi_key!= NULL
    //              range scan from min to the hi_key
    //      (3) lo_key!= NULL, hi_key = NULL
    //              range scan from the lo_key to max
    //      (4) lo_key!= NULL, hi_key!= NULL, lo_key = hi_key
    //              exact match ( might not unique)
    //      (5) lo_key!= NULL, hi_key!= NULL, lo_key < hi_key
    //              range scan from lo_key to hi_key

    int keysize() {
		return header.key_size;
	}
    
  private:
	bool initialised;
	std::string filename;

	PageId header_pageid;
	struct header_t {
		int m_number;

		PageId root;
		PageId leftmost;
		AttrType key_type;
		int key_size;

		static const int magic_number = 0x12345678;

		static void init(header_t* header, PageId root, AttrType key_type, int key_size) {
			header->m_number = magic_number;
			header->root = root;
			header->leftmost = root;
			header->key_type = key_type;
			header->key_size = key_size;
		}

		static Status init(header_t* header, void* page) {
			*header = *(header_t*) page;	
			if (header->m_number != magic_number) {
				return MINIBASE_FIRST_ERROR(BTREE, BTREE_HEADER_INCORRECT_MAGIC);
			}

			return OK;
		}
	} header;

	Status flush_header();
	
	/* assume header_pageid is initialised */
	Status init_from_existing_file();
};

#endif
