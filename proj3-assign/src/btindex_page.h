/* -*- C++ -*- */
/*
 * btindex_page.h - definition of class BTIndexPage for Mini{base|rel} project.
 *
 */

#ifndef BTINDEX_PAGE_H
#define BTINDEX_PAGE_H

#include "minirel.h"
#include "page.h"
#include "sorted_page.h"
#include "bt.h"
#include <string.h>

// Define your error code for index page here
enum btIndexErrCodes  {
	BTINDEX_NOTIMPLEMENTED = 0,
	BTINDEX_KEYTOOLONG,
	BTINDEX_EMPTY, 

	BTINDEX_FATAL_ERROR,
	BTINDEX_SPLITFAILED,
};

/**
 * [left link] key1 [right link 1] key2 [right link2]
 * key1 <= keys in right link 1 < key2 or 
 * (key1 == keys in right link 1 == key2)
 */
class BTIndexPage : public SortedPage {
 private:
   // No private variables should be declared.

 public:

// In addition to initializing the  slot directory and internal structure
// of the HFPage, this function sets up the type of the record page.

   void init(PageId pageNo)
   { HFPage::init(pageNo); set_type(INDEX); }

// ------------------- insertKey ------------------------
// Inserts a <key, page pointer> value into the index node.
// This is accomplished by a call to SortedPage::insertRecord()
// This function sets up the recPtr field for the call to
// SortedPage::insertRecord()
   
   Status insertKey(const void *key, AttrType key_type,PageId pageNo, RID& rid);


// ------------------- OPTIONAL: deletekey ------------------------
// this is optional, and is only needed if you want to do full deletion
   Status deleteKey(const void *key, AttrType key_type, RID& curRid);

// ------------------ get_page_no -----------------------
// This function encapsulates the search routine to search a
// BTIndexPage. It uses the standard search routine as
// described in the textbook, and returns the page_no of the
// child to be searched next.

/* if duplicates are allowed, the page no. of the highest matching
 * page is returned. 
 * caller should not call this function if all the records need to
 * be iterated through.
 **/
   Status get_page_no(const void *key, AttrType key_type, PageId & pageNo);

    
// ------------------- Iterators ------------------------
// The two functions get_first and get_next provide an
// iterator interface to the records on a BTIndexPage.
// get_first returns the first <key, pageNo> pair from the page,
// while get_next returns the next pair on the page.
// These functions make calls to HFPage::firstRecord() and
// HFPage::nextRecord(), and then split the flat record into its
// two components: namely, the key and pageNo.
// Should return NOMORERECS when there are no more pairs.

   Status get_first(RID& rid, void *key, PageId & pageNo);
   Status get_next (RID& rid, void *key, PageId & pageNo);

// ------------------- Left Link ------------------------
// You will recall that index pages have a left-most
// pointer that is followed whenever the search key value
// is less than the least key value in the index node. The
// previous page pointer is used to implement the left link.

   PageId getLeftLink(void) { return getPrevPage(); }
   void   setLeftLink(PageId left) { setPrevPage(left); }

    
   // The remaining functions of SortedPage are still visible.

   /**
	* Binary search for the no. of the highest slot, i, so that
	* the key is in the range of the page pointed by the right link
	* of the slot, or -1 if it's in the range of the page pointed by
	* the left link of this page.
	*/
   Status __search_for_highest_slot_no(const void* key, AttrType key_type, int& slot_no);
	
   /**
	* Binary search for the no. of the lowest slot, i, so that,
	* the key is in then range of the page pointed by the right link 
	* of the slot, or -1 if it's in the range of the page pointed by 
	* the left link of this page.
	*/
	Status __search_for_lowest_slot_no(const void* key, AttrType key_type, int& slot_no);

	Status __search_for_lowest_slot_rid(const void* key, AttrType key_type, RID& rid) {
		rid.pageNo = page_no();
		return __search_for_lowest_slot_no(key, key_type, rid.slotNo);
	}

	Status __search_for_highest_slot_rid(const void* key, AttrType key_type, RID& rid) {
		rid.pageNo = page_no();
		return __search_for_highest_slot_no(key, key_type, rid.slotNo);
	}

	Status __get_key_data(RID rid, void* key, PageId& pageNo) {
		char* entry;
		int entry_size;
		Status status = returnRecord(rid, entry, entry_size);
		if (status != OK) {
			return MINIBASE_CHAIN_ERROR(BTLEAFPAGE, status);
		}

		get_key_data(key, (Datatype*) &pageNo, 
				(KeyDataEntry*) entry, entry_size, INDEX);
		return OK;
   }

   /**
	* Splits the page with the <key, pageNo> inserted 
	* into two pages with roughly equal size of records.
	* The pageno of the new page is returned in new_page.
	* first_key should be a buffer that is large enough to hold the 
	* first key in the new page.
	*
	* key can be the same pointer as first_key since key is copied upon
	* entering this function.
	*
	* The rid of the inserted record is stored in parameter ``rid''.
	* If the new link is the left link of the new page, then rid.slotNo == -1.
	*/
   Status split(const void* key, AttrType key_type, PageId pageNo, 
		   PageId& new_page, void* first_key, 
		   RID& rid);
};

#endif
