/* -*- C++ -*- */
/*
 * btleaf_page.h - definition of class BTLeafPage for Minibase project.
 *
 */

#ifndef BTLEAF_PAGE_H
#define BTLEAF_PAGE_H

#include "minirel.h"
#include "page.h"
#include "sorted_page.h"
#include "bt.h"
#include "btindex_page.h"

enum btLeafErrCodes {
	BTLEAF_NOTIMPLEMENTED = 0, 
	BTLEAF_KEYTOOLONG,
	BTLEAF_KEYNOTFOUND,
	BTLEAF_SPLITFAILED,
};

class BTLeafPage : public SortedPage {
  
 private:
   // No private variables should be declared.

 public:


// In addition to initializing the  slot directory and internal structure
// of the HFPage, this function sets up the type of the record page.

   void init(PageId pageNo)
   { HFPage::init(pageNo); set_type(LEAF); }

// ------------------- insertRec ------------------------
   // READ THIS DESCRIPTION CAREFULLY. THERE ARE TWO RIDs
   // WHICH MEAN TWO DIFFERENT THINGS.
// Inserts a <key, dataRid> value into the leaf node. This is
// accomplished by a call to SortedPage::insertRecord()
// This function sets up the recPtr field for the call
// to SortedPage::insertRecord() 
//
// Parameters:
//   o key - the key value of the data record.
//
//   o key_type - the type of the key.
//
//   o dataRid - the rid of the data record. This is
//               stored on the leaf page along with the
//               corresponding key value.
//
//   o rid - the rid of the inserted leaf record data entry, i.e., the
//     <key, dataRid> pair
   
   Status insertRec(const void *key, AttrType key_type, RID dataRid, RID& rid);

// ------------------- Iterators ------------------------
// The two functions get_first and get_next provide an
// iterator interface to the records on a BTLeafPage.
// get_first returns the first <key, dataRid> pair from the page,
// while get_next returns the next pair on the page.
// These functions make calls to HFPage::firstRecord() and
// HFPage::nextRecord(), and then split the flat record into its
// two components: namely, the key and dataRid.
// Should return NOMORERECS when there are no more pairs

   Status get_first(RID& rid, void *key, RID & dataRid);
   Status get_next (RID& rid, void *key, RID & dataRid);


// ------------------ get_data_rid -----------------------
// This function performs a sequential search (or a
// binary search if you are ambitious) to find a data entry
// of the form <key, dataRid>, where key is given in the call.
// It returns the dataRid component of the pair; note that this
// is the rid of the DATA record, and NOT the rid of the data entry!
//
// If there are duplicates, the dataRid of the record with matching key
// and the highest slot no. in this page is returned.

   Status get_data_rid(void *key, AttrType attrtype, RID & dataRid);
	
   /**
	* Binary search for the lowest entry rid the key of which matches the
	* search key.
	*
	* @returns DONE if key is not found in this page; OK if key is found;
	*	error subsystem if any error occurs
	*/
   Status __search_for_lowest_entry_rid(const void* key, AttrType attrtype, RID& entryRid);
	
   /**
	* Binary search for the highest entry rid the key of which matches the 
	* search key.
	*
	* @returns DONE if key is not found in this page; OK if key is found;
	*	error subsystem if any error occurs
	*/
   Status __search_for_highest_entry_rid(const void* key, AttrType attrtype, RID& entryRid);

   /**
	* Search for the lowest entry rid the key of which is greater than or equal to
	* the search key.
	*
	* if nothing is found, rid.slotNo == slotCnt + 1
	*/
   Status __search_for_lower_bound(const void* key, AttrType attrtype, RID& entryRid);
	
   /**
	* Search for the highest entry rid the key of which is smaller than or equal to
	* the search key.
	*
	* if nothing is found, rid.slotNo == -1
	*/
   Status __search_for_upper_bound(const void* key, AttrType attrtype, RID& entryRid);

	Status __get_key_data(RID rid, void* key, RID& dataRid) {
		char* entry;
		int entry_size;
		Status status = returnRecord(rid, entry, entry_size);
		if (status != OK) {
			return MINIBASE_CHAIN_ERROR(BTLEAFPAGE, status);
		}

		get_key_data(key, (Datatype*) &dataRid, 
				(KeyDataEntry*) entry, entry_size, LEAF);
		return OK;
   }

   /**
	* Splits the page with <key, dataRid> inserted 
	* into two pages with roughly equal size of records.
	* The pageno of the new page is returned in new_page.
	* first_key should be a buffer that is large enough to hold the 
	* first key in the new page.
	*
	* key can be thesame pointer as first_key since key is copied upon
	* entering this function.
	*/
   Status split(const void* key, AttrType key_type, RID dataRid, 
		   PageId& new_page, void* first_key, RID& rid);
};

/**
 * Maintain the horizontal links between leaf pages.
 * 
 * @param leaf_pageid	the newly splited leaf page
 * @param slot_rid		the rid of the entry that points to the leaf page
 */
Status link_leaf_page(PageId leaf_pageid, RID slot_rid);

#endif
