#include "sorted_page.h"
#include <cstring>
using namespace std;

int MINIBASE_RESTART_FLAG = 0;

int main() {
	SortedPage* s = new SortedPage;
	s->init(1);

	char key[MAX_KEY_SIZE1];
	memset(key, 0x30, MAX_KEY_SIZE1 - 1);
	key[MAX_KEY_SIZE1 - 1] = 0;
	
	int entry_size;
	PageId p = 1;
	char entry[MAX_KEY_SIZE1 + 20];
	make_entry((KeyDataEntry*) entry,
			attrString,
			key,
			INDEX,
			*(Datatype*)&p,
			&entry_size);

	printf("entry size = %d\n", entry_size);
	printf("free space = %d\n", s->free_space());
	printf("available space = %d\n", s->available_space());
	printf("number of records = %d\n", s->numberOfRecords());

	Status status = OK;
	RID rid;
	while (status == OK) {
		status = s->insertRecord(attrString, entry, entry_size, rid);
		printf("insert 1, free space = %d\n", s->free_space());
		printf("	available space = %d\n", s->available_space());
		printf("	number of records = %d\n", s->numberOfRecords());
	}

	MINIBASE_FIRST_ERROR(SORTEDPAGE, 0);

	minibase_errors.show_errors();

	return 0;
}
