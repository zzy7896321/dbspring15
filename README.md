Database course projects at SJTU
===================================

## Project 1 heap file page

## Project 2 buffer manager

## Project 3 B+ tree

Duplicate keys are allowed. An entry with a duplicate key is
retained and is put to the right of the entries with the 
duplicate keys. Delete removes all entries that
match the (key, rid) parameter.

## Project 4 External Sorting

With qsort and merge sort.

Since we are using qsort_r, glibc is assumed to be used.

Merge phase is implemented without a heap, which is not possible
with the interfaces inside HeapFile without violate the memory
limit, so the performance will degrade if the amount of buffer
is set too large.

As documented in Sort.C, the Scan::position will fail to move to
the previous returned record if it is on the boundary of a page.
So all the returned entries has to be cached and resulting in 
a amt_of_buf pages overhead in worst case.

