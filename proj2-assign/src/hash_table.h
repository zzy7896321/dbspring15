#ifndef HASH_TABLE_H
#define HASH_TABLE_H

#include <algorithm>
#include <utility>

/* A hash table with fixed number of buckets. */
template<typename K, typename V, class H, unsigned bucket_size>
class HashTable {
public:
	typedef int __assert_bucket_size_is_greater_than_0[bucket_size > 0 ? 1 : -1];

	typedef K key_type;
	typedef V value_type;
	typedef H hash_func;
	typedef unsigned hash_type;

	HashTable() { 
		std::fill(node, node + bucket_size, (ListNode*) 0);	
	}

	~HashTable() {
		ListNode* tmp = 0;
		for (unsigned i = 0; i < bucket_size; ++i) 
		while (node[i]){
			tmp = node[i];
			node[i] = node[i]->next;
			delete tmp;
		}
	}

	bool insert(const key_type& key, const value_type& value) {
		unsigned h = hash_func()(key) % bucket_size;
		ListNode* node_p = __find_by_key(key, h).second;
		if (node_p) return false;
		node[h] = new ListNode(key, value, node[h]);		
		return true;
	}

	value_type* find(const key_type& key) {
		ListNode* node_p = __find_by_key(key).second;
		if (node_p) return &(node_p->value);
		return 0;
	}

	void remove(const key_type& key) {
		std::pair<ListNode**, ListNode*> ptrs = __find_by_key(key);	
		ListNode** prev_ptr = ptrs.first;
		ListNode* now = ptrs.second;
		if (now) {
			*prev_ptr = now->next;
			delete now;
		}
	}

private:
	/* delete the copy constructor and = opeartor */
	HashTable(const HashTable&);
	HashTable& operator=(const HashTable&);

	struct ListNode {
		ListNode(const key_type& k, const value_type& v, ListNode* n = 0)
			: key(k), value(v), next(n) {}
		
		K key;
		V value;
		ListNode* next;
	};

	std::pair<ListNode**, ListNode*> __find_by_key(const key_type& key) {
		return __find_by_key(key, hash_func()(key) % bucket_size);
	}

	std::pair<ListNode**, ListNode*> __find_by_key(const key_type& key, unsigned h) {
		ListNode** prev_ptr = &node[h];
		ListNode* now = node[h];
		for (; now && now->key != key; prev_ptr = &(now->next), now = now->next);
		return std::make_pair(prev_ptr, now);
	}

	ListNode* node[bucket_size];
};

template<int a, int b>
struct IntHash {
	unsigned operator()(int x) {
		return a * x + b;	
	}
};

#endif	// HASH_TABLE_H
