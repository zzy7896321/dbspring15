#include "hash_table.h"
#include <cstdio>
#include <inttypes.h>
using namespace std;

inline void print_int_ptr(int* x) {
	if (x)
		printf("*(0x%016lx) = %d\n", (unsigned long int) x, *x);
	else
		printf("NULL\n");
}

int main() {
	HashTable<int, int, IntHash<0, 0>, 7u> h;
	
	for (int i = 0; i < 5; ++i) {
		printf("insert (%d, %d)\n", i, i + 5);
		h.insert(i, i + 5);
	}

	for (int i = 0; i < 5; ++i) {
		printf("h.find(%d): ", i);	
		print_int_ptr(h.find(i));
		printf("h.find(%d): ", i + 5);	
		print_int_ptr(h.find(i + 5));
	}
	
	printf("remove 5 .. 9\n");
	for (int i = 0; i < 5; ++i) {
		h.remove(i + 5);
	}

	for (int i = 0; i < 5; ++i) {
		printf("h.find(%d): ", i);	
		print_int_ptr(h.find(i));
		printf("h.find(%d): ", i + 5);	
		print_int_ptr(h.find(i + 5));
	}

	printf("remove 0 .. 2\n");
	for (int i = 0; i < 3; ++i) {
		h.remove(i);
	}

	for (int i = 0; i < 5; ++i) {
		printf("h.find(%d): ", i);	
		print_int_ptr(h.find(i));
		printf("h.find(%d): ", i + 5);	
		print_int_ptr(h.find(i + 5));
	}
	
	printf("remove 3 .. 4\n");
	for (int i = 3; i < 5; ++i) {
		h.remove(i);
	}

	for (int i = 0; i < 5; ++i) {
		printf("h.find(%d): ", i);	
		print_int_ptr(h.find(i));
		printf("h.find(%d): ", i + 5);	
		print_int_ptr(h.find(i + 5));
	}
	return 0;
}
