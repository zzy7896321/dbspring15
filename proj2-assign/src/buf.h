///////////////////////////////////////////////////////////////////////////////
/////////////  The Header File for the Buffer Manager /////////////////////////
///////////////////////////////////////////////////////////////////////////////


#ifndef BUF_H
#define BUF_H

#include "db.h"
#include "page.h"


#define NUMBUF 20   
// Default number of frames, artifically small number for ease of debugging.

#define HTSIZE 7
// Hash Table size
//You should define the necessary classes and data structures for the hash table, 
// and the queues for LSR, MRU, etc.

#define BUF_HASH_A 3
#define BUF_HASH_B 5
#include "hash_table.h"

#if __cplusplus >= 201103L
#	include <type_traits>
#endif

/*******************ALL BELOW are purely local to buffer Manager********/

// You should create enums for internal errors in the buffer manager.
enum bufErrCodes  { 
	NO_AVAILABLE_FRAME = 0,
	PAGE_NOT_FOUND,
	PIN_COUNT_UNDERFLOW,
	FREE_PINNED_PAGE,
	SECOND_ERROR_NEW_PAGE,
};

class Replacer;

class BufMgr {

private: // fill in this area
	typedef IntHash<BUF_HASH_A, BUF_HASH_B> buf_hash_func;
	typedef HashTable<PageId, int, buf_hash_func, HTSIZE>	buf_hash_table_t;
	
	int num_buf;

	buf_hash_table_t page_to_frame;

	struct List {
		List* prev;
		List* next;
	} *loved, *hated, *free;

	struct FrameInfo{
		PageId page_id;
		List list_node;
		int pin_count;
		bool dirty;
		bool loved;
	} *frame_info;

#if __cplusplus >= 201103L
	static_assert(std::is_standard_layout<FrameInfo>::value, "FrameInfo is not standard-layout.");
#endif
	
	Status get_frame(int &frame_id);

	Status flush_frame(int frame_id);

	static List* new_list_head() {
		List* l = new List;
		return l->prev = l->next = l;
	}

	static bool is_empty(List* head) {
		return head->next == head;
	}

	static List* insert_to_head(List* l, List* list_head) {
		l->prev = list_head;
		l->next = list_head->next;
		list_head->next->prev = l;
		list_head->next = l;
		return l;
	}

	static List* insert_to_tail(List* l, List* list_head) {
		return insert_to_head(l, list_head->prev);
	}

	static List* remove_from_list(List* l) {
		l->prev->next = l->next;
		l->next->prev = l->prev;
		return l;
	}
	
public:

    Page* bufPool; // The actual buffer pool

    BufMgr (int numbuf, Replacer *replacer = 0); 
    // Initializes a buffer manager managing "numbuf" buffers.
	// Disregard the "replacer" parameter for now. In the full 
  	// implementation of minibase, it is a pointer to an object
	// representing one of several buffer pool replacement schemes.

    ~BufMgr();           // Flush all valid dirty pages to disk

    Status pinPage(PageId PageId_in_a_DB, Page*& page, int emptyPage=0);
        // Check if this page is in buffer pool, otherwise
        // find a frame for this page, read in and pin it.
        // also write out the old page if it's dirty before reading
        // if emptyPage==TRUE, then actually no read is done to bring
        // the page

    Status unpinPage(PageId globalPageId_in_a_DB, int dirty, int hate);
        // hate should be TRUE if the page is hated and FALSE otherwise
        // if pincount>0, decrement it and if it becomes zero,
        // put it in a group of replacement candidates.
        // if pincount=0 before this call, return error.

    Status newPage(PageId& firstPageId, Page*& firstpage, int howmany=1); 
        // call DB object to allocate a run of new pages and 
        // find a frame in the buffer pool for the first page
        // and pin it. If buffer is full, ask DB to deallocate 
        // all these pages and return error

    Status freePage(PageId globalPageId); 
        // user should call this method if it needs to delete a page
        // this routine will call DB to deallocate the page 

    Status flushPage(PageId pageid);
        // Used to flush a particular page of the buffer pool to disk
        // Should call the write_page method of the DB class

    Status flushAllPages();
	// Flush all pages of the buffer pool to disk, as per flushPage.

    /* DO NOT REMOVE THIS METHOD */    
    Status unpinPage(PageId globalPageId_in_a_DB, int dirty=FALSE)
        //for backward compatibility with the libraries
    {
      return unpinPage(globalPageId_in_a_DB, dirty, FALSE);
    }
};

#endif
