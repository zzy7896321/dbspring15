#ifndef PAGE_H
#define PAGE_H

#include "minirel.h"

const PageId INVALID_PAGE = -1;
const int MAX_SPACE = MINIBASE_PAGESIZE;

#if __cplusplus >= 201103L
#	include <type_traits>
#	include <cstddef>
#endif

class Page
{
public:
    Page();
    ~Page();


private:
    char data[MAX_SPACE];

#if __cplusplus >= 201103L
	void __unused_func() {
		static_assert(offsetof(Page, data) == 0, "offset of data in Page is not 0");
		static_assert(std::is_standard_layout<Page>::value, "Page is not standard-layout");
		/* Page is not POD */
		//static_assert(std::is_pod<Page>::value, "Page is not POD");
	}
#endif
};

#endif
